#from django.test import TestCase

# Create your tests here.
import p_image
import pickle as pk
from scipy.misc import imread
from matplotlib import pyplot as plt
import numpy as np

W1 = pk.load(open('weights/W1', 'rb'), encoding='latin1')
b1 = pk.load(open('weights/b1', 'rb'), encoding='latin1')
W2 = pk.load(open('weights/W2', 'rb'), encoding='latin1')
b2 = pk.load(open('weights/b2', 'rb'), encoding='latin1')
W3 = pk.load(open('weights/W3', 'rb'), encoding='latin1')
b3 = pk.load(open('weights/b3', 'rb'), encoding='latin1')

im = p_image.p_image(imread('/home/hung/Downloads/Drawing (5).png', 'L'))
#print(im)
#plt.imshow(im.reshape((28, 28)))
#plt.show()

def relu(a):
    b = np.zeros(a.shape, dtype=a.dtype)
    for i in range(len(a)):
        if a[i] > 0:
            b[i] = a[i]
    return b

out1 = relu(im.dot(W1) + b1)
out2 = relu(out1.dot(W2) + b2)
Y = out2.dot(W3) + b3
print(np.argmax(Y))

