from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from django.core.files.storage import default_storage
from scipy.misc import imread
import numpy as np 
from .p_image import p_image
import pickle as pk 

def index(request):#the first page
    template = loader.get_template('recognize/index.html')
    return HttpResponse(template.render({}, request))

def predict(request):#predicting view
    #receive image file
    file = request.FILES['image']
    #delete all file in images folder
    _, fls = default_storage.listdir('recognize/static/images')
    for f in fls:
        default_storage.delete('recognize/static/images/' + f)
    #save the uploaded file
    path = default_storage.save('recognize/static/images/im', file)
    #read and recognize digit image
    try:
        im = imread(file, 'L')
    except:
        return render(request, 'recognize/index.html', {'invalid_file_format': True})
    digit = recog(im)
    return render(request, 'recognize/index.html', {'digit': digit})

def relu(a):#apply ReLU after every layer
    b = np.zeros(a.shape, dtype=a.dtype)
    for i in range(len(a)):
        if a[i] > 0:
            b[i] = a[i]
    return b

def recog(im):#recognize an image
    im = p_image(im)
    W1 = pk.load(open('recognize/weights/W1', 'rb'), encoding='latin1')
    b1 = pk.load(open('recognize/weights/b1', 'rb'), encoding='latin1')
    W2 = pk.load(open('recognize/weights/W2', 'rb'), encoding='latin1')
    b2 = pk.load(open('recognize/weights/b2', 'rb'), encoding='latin1')
    W3 = pk.load(open('recognize/weights/W3', 'rb'), encoding='latin1')
    b3 = pk.load(open('recognize/weights/b3', 'rb'), encoding='latin1')

    out1 = relu(im.dot(W1) + b1)
    out2 = relu(out1.dot(W2) + b2)
    Y = out2.dot(W3) + b3

    return str(np.argmax(Y))