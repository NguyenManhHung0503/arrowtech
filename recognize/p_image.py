import numpy as np 
from scipy.misc import imread, imresize
from os import listdir
import os
from scipy.ndimage.measurements import center_of_mass

def p_image(im):
    im = forcus(im)
    center = center_of_mass(im)
    if type(center) is list:
        center = center[0]
    cx = int(np.floor(center[0]))
    cy = int(np.floor(center[1]))
    return_im = np.zeros((28, 28))
    t = max(0, 14 - cx)
    b = min(27, 14 + 19 - cx)
    l = max(0, 14 - cy)
    r = min(27, 14 + 19 - cy)
    return_im[t : b + 1, l : r + 1] = im[cx - (14 - t) : cx + b - 14 + 1, cy - (14 - l) : cy + r - 14 + 1]
    return return_im.reshape(-1)

def forcus(im):
    t = 0
    while np.sum(im[t]) == 0:
        t += 1
    b = im.shape[0] - 1
    while np.sum(im[b]) == 0:
        b -= 1
    l = 0
    while np.sum(im[:, l]) == 0:
        l += 1
    r = im.shape[1] - 1
    while np.sum(im[:, r]) == 0:
        r -= 1
    im = 255 - imresize(im[t: b + 1, l : r + 1], (20, 20))
    return im